#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys

from flask_restful import reqparse, abort, Resource
from flask import request
from flask import Response
from bson.objectid import ObjectId

import json
import datetime
import numpy as np
from pyproj import Proj, Transformer

from app.auth.util import login_required

from app.db import mongo_db
from app.util import DBObject
import itertools
import json
from app.util import convert_dict_to_JSON
from bson.objectid import ObjectId
# from app.white_lines import white_line
from shapely.geometry import Polygon as SPolygon, Point, LineString

import os

wgs2utm = Transformer.from_crs(4326, 3096)
utm2wgs = Transformer.from_crs(3096, 4326)

def layers_api(app):
    """
    API functions
    """
    app.route('/api/map-light/layers', methods=['PUT'])(api_map_light_layers)
    app.route('/api/map-light/layers_find', methods=['PUT'])(api_map_light_layers_find)

    app.route('/api/map-light/update_feature_geo', methods=['PUT'])(api_map_light_update_feature_geo)

    app.route('/api/map-light/add_feature_geo', methods=['PUT'])(api_map_light_add_feature_geo)
    app.route('/api/map-light/remove_feature_geo', methods=['PUT'])(api_map_light_remove_feature_geo)


def api_map_light_layers():
    st = datetime.datetime.now()
    obj = request.json
    box = obj['box']
    skip = obj['skip']
    limit = obj['limit']
    print(skip, limit)
    query = {
        '$or':[
            {
                'geo': {
                    '$geoWithin': {
                        '$geometry': {
                            'type': "Polygon",
                            'coordinates': box
                        }
                    }
                }
            },
            {
                'geo': {
                    '$geoIntersects': {
                        '$geometry': {
                            'type': "Polygon",
                            'coordinates': box
                        }
                    }
                }
            }
        ]
    }
    print(query)
    rez = []
    query['visible'] = True
    query['layer'] =  obj['layer']#{'$in': obj['visible_layers']}
    print(query)
    cursor = mongo_db.layers_light.find(query).skip(skip).limit(limit)
    count = cursor.count()
    print('layers count:', count)
    for el in cursor:
        el['_id'] = str(el['_id'])
        rez.append(el)
    et = datetime.datetime.now()
    print('layers find:', et-st, count)
    return Response(json.dumps({'rez': rez, 'count': count}), mimetype='application/json')

def api_map_light_layers_find():
    query = request.json
    print(query)
    rez = []
    cursor = mongo_db.layers_light.find(query).limit(1000)
    print('layers', cursor.count())
    for el in cursor:
        el['_id'] = str(el['_id'])
        rez.append(el)
    # print(rez[0])
    return Response(json.dumps({'rez': rez}), mimetype='application/json')

def api_map_light_update_feature_geo():
    feature = request.json
    q = {'_id': ObjectId(feature['_id'])}
    #doc = {'$set':{'geo': feature['geo']}}
    del feature['_id']
    feature['visible'] = True
    doc = {'$set':feature}
    mongo_db.layers_light.update(q, doc)
    return Response(json.dumps({'rez': 'ok'}), mimetype='application/json')


def api_map_light_add_feature_geo():
    feature = request.json
    feature['visible'] = True
    mongo_db.layers_light.insert(feature)
    return Response(json.dumps({'rez': 'ok'}), mimetype='application/json')


def api_map_light_remove_feature_geo():
    feature = request.json
    q = {'_id': ObjectId(feature['_id'])}
    #doc = {'$set':{'geo': feature['geo']}}
    feature['__id'] = feature['_id']
    del feature['_id']
    mongo_db.layers_light.remove(q)
    return Response(json.dumps({'rez': 'ok'}), mimetype='application/json')

