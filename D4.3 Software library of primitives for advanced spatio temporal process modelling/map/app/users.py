#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys

from flask_restful import reqparse, abort, Resource
from flask import request
from flask import Response
from bson.objectid import ObjectId

import json

from app.auth.util import token_generator
from app.auth.util import USERS
from app.auth.util import TOKEN_USERS
from app.auth.util import login_required
from app.auth.util import check_password

from app.db import mongo_db
from app.util import DBObject
import itertools
import json
from bson.objectid import ObjectId


def users_api(app):
    """
    API functions

    """
    app.route('/api/users')(get_all_users)

    app.route('/api/users/all')(api_users_get)
    app.route('/api/users/add', methods=['PUT'])(api_users_add_put)
    app.route('/api/users/remove', methods=['PUT'])(api_remove_user)

    app.route('/api/users/by_token/<token>', methods=['GET'])(user_by_token)
    app.route('/api/users/login', methods=['GET'])(user_login)

def user_login():
    print('--------------- tu sam ------------------')
    username = request.args.get("username")
    password = request.args.get("password")
    q = {'username': username, 'password': password}
    cursor = mongo_db.users.find(q)
    c = cursor.count()
    if c == 1:
        for usr in cursor:
            usr['_id'] = str(usr['_id'])
            key = str(usr['_id'])
            token = token_generator()
            USERS[token] = usr
            TOKEN_USERS[key] = token
            return token, 200
    else:
        return Response(json.dumps({'res':'failure'}), mimetype='application/json')

def user_by_token(token):
    if token in USERS:
        return Response(json.dumps({'res': USERS[token]}), mimetype='application/json')
    else:
        return Response(json.dumps({'res': 'failure'}), mimetype='application/json')

@login_required
def get_all_users():
    cursor = mongo_db.adminusers.find({})
    all = []
    for el in cursor:
        el['_id'] = str(el['_id'])
        all.append(el)
    return Response(json.dumps(all), mimetype='application/json')


def api_users_get():
    cursor = mongo_db.users.find({})
    rez = []
    for el in cursor:
        el['_id'] = str(el['_id'])
        rez.append(el)
    return Response(json.dumps({'rez': rez}), mimetype='application/json')


def api_users_add_put():
    obj = request.json
    if '_id' in obj:
        q = {'_id': ObjectId(obj['_id'])}
        obj['_id'] = ObjectId(obj['_id'])
        mongo_db.users.update(q, obj)
        obj['_id'] = str(obj['_id'])
    else:
        id = mongo_db.users.insert(obj)
        obj['_id'] = str(id)
    return Response(json.dumps({'rez': obj}), mimetype='application/json')


def api_remove_user():
    feature = request.json
    q = {'_id': ObjectId(feature['_id'])}
    feature['__id'] = feature['_id']
    del feature['_id']
    mongo_db.users.remove(q)
    return Response(json.dumps({'res': 'ok'}), mimetype='application/json')
