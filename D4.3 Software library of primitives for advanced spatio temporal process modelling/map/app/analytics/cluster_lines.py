#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys

from bson.objectid import ObjectId
import json
import itertools
import json
import numpy as np
from bson.objectid import ObjectId
from pyproj import  Proj, transform


from pymongo import MongoClient
import os

projections = {
    'EPSG:4326': Proj(init='epsg:4326'), 
    'EPSG:25832': Proj(init='epsg:25832'),
    'EPSG:27700': Proj(init='epsg:27700')
}

client = MongoClient("localhost", 27017, connect=False)
mongo_db = client.vision021  

def proj4(inProj, outProj, point):
    x, y = transform(projections[inProj], projections[outProj], point[0], point[1])
    if len(point)== 3:
        return [x, y, point[2]]
    else:
        return [x, y, 0]

def wgs2utm(ip):
    p = proj4('EPSG:4326', 'EPSG:25832',[ip[0], ip[1]] ) 
    if len(ip) ==3:
        return np.array([p[0], p[1], ip[2]])
    else:
        return np.array([p[0], p[1]])

def utm2wgs(ip):
    p = proj4('EPSG:25832', 'EPSG:4326', [ip[0], ip[1]] ) 
    if len(ip) ==3:
        return np.array([p[0], p[1], ip[2]])
    else:
        return np.array([p[0], p[1]])

front = []
visited = {}
query = {'_id': ObjectId('5e0f466b18d229c7357f0367')}
cursor = mongo_db.layers.find(query)
count = cursor.count()
for el in cursor:
    el['_id'] = str(el['_id'])
    print(el)
    A = el['geo']['coordinates'][0]
    B = el['geo']['coordinates'][1]
    front.append(A)
    front.append(B)
    visited[str(A)] = True
    visited[str(B)] = True
    A = wgs2utm(A)
    B = wgs2utm(B)
    d = np.linalg.norm(A-B)
    print(d)

coordinates = []
cc = 0
radius = 1.5
while len(front)>0:
    P = front.pop(0)
    coordinates.append(P)
    print(cc, P, len(front))
    query = {
        "layer": "lane-border",
        "geo":{
            "$near": {
                "$geometry": {
                    "type": "Point" ,
                    "coordinates": P
                },
                "$maxDistance": radius,
                "$minDistance": 0
            }
        }
    }
    cursor = mongo_db.layers.find(query)
    count = cursor.count()
    for el in cursor:
        el['_id'] = str(el['_id'])
        # print(cc, el['_id'])
        A = el['geo']['coordinates'][0]
        B = el['geo']['coordinates'][1]
        if str(A) not in visited:
            front.append(A)
            visited[str(A)] = True

        if str(B) not in visited:
            front.append(B)
            visited[str(B)] = True

    cc += 1
    if cc>1500:
        break


mongo_db.layers.remove({'layer': 'debug','id': 'test'})

feature = {
    'layer': 'debug',
    'id': 'test',
    'geo': {'type':'LineString', 'coordinates':coordinates}
}
# print(feature)
mongo_db.layers.insert(feature)
