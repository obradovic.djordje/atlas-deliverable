const path = require('path'),
      isDevelopment = process.env.NODE_ENV === 'development',
      MiniCssExtractPlugin = require('mini-css-extract-plugin'),
      { CleanWebpackPlugin } = require('clean-webpack-plugin'),
      HtmlWebpackPlugin = require('html-webpack-plugin'),
      HtmlBeautifyPlugin = require('html-beautify-webpack-plugin');
      webpack = require('webpack');

module.exports = {
    mode: isDevelopment ? 'development' : 'production',
    entry: './src/js/script.js',
    output: {
        filename: 'bundle.js',
        path: __dirname + '/dist',
        publicPath: '/',
    },
    devServer: {
        contentBase: __dirname + '/src',
        publicPath: '/',
        inline: true,
        hot: true,
        watchContentBase: true
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: isDevelopment ? '[name].[hash].css' : '[name].[hash].css',
            chunkFilename: isDevelopment ? '[id].[hash].css' : '[id].[hash].css'
        }),
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: 'src/template.html',
            filename: 'index.html'
        }),
        new HtmlBeautifyPlugin({
            config: {
                html: {
                    end_with_newline: true,
                    indent_size: 2,
                    indent_with_tabs: true,
                    indent_inner_html: true,
                    preserve_newlines: true,
                    unformatted: ['p', 'i', 'b', 'span']
                }
            },
            replace: [' type="text/javascript"']
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ],
    module: {
        rules: [
            {
                test: /\.s(a|c)ss$/,
                loader: [
                    isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: isDevelopment
                        }
                    },
                ]
            },
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx', '.scss']
    }
};