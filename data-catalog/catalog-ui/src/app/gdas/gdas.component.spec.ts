import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GdasComponent } from './gdas.component';

describe('GdasComponent', () => {
  let component: GdasComponent;
  let fixture: ComponentFixture<GdasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GdasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GdasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
