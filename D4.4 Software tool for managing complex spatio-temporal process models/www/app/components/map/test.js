class Test {
    calculatePowers() {
      return [1,2,3].map(n => n ** 2);
    }
  }

export default Test;