import numpy as np
import pandas as pd
import datetime
import matplotlib.pyplot as plt
import matplotlib as mpl

# https://www.epa.gov/criteria-air-pollutants/naaqs-table
# https://www.airnow.gov/sites/default/files/2020-05/aqi-technical-assistance-document-sept2018.pdf

# "88101 PM2.5 FRM/FEM Mass",  # 'Micrograms/cubic meter (LC)']
# "81102", # PM10  Micrograms/cubic meter (25 C)
# "44201 Ozone", # Parts per million
# "42101 CO", # Parts per million
# "42401 SO2", # Parts per billion
# "42602 NO2" # Parts per billion

PSI_BREAKPOINTS = { # 
    '81102' : [ # mikro g per m3
        [0,     50,  150,  350,  420,   500,  600],
        [0,     50,  100,  200,  300,   400,  500]
    ],
    '42401 SO2' : [ # ppm
        [0, 0.03,  0.14, 0.3, 0.6, 0.8, 1],
        [0,   50,  100,  200,  300,   400,  500],
    ],
    '42101 CO' : [ # ppm
          [0,   4.5,    9,   15,   30,    40, 50],
          [0,    50,  100,  200,  300,   400,  500]
    ],
    '42602 NO2' : [ # ppm
        [0, 0.6,  1.2, 1.6, 2],
        [0, 200,  300, 400, 500]
    ],
    '44201 Ozone' : [ # ppm
        [0, 0.06, 0.12,  0.2,  0.4, 0.5, 0.6], 
        [0,   50,  100,  200,  300, 400, 500]
    ],
}

units = {
    "81102": "Micrograms/cubic meter (25 C)",
    "44201 Ozone": "ppm",
    "42101 CO": "ppm",
    "42401 SO2": "ppb",
    "42602 NO2": "ppb"
}

scales = {
    "81102": 1, # PM10  Micrograms/cubic meter (25 C)
    "44201 Ozone": 1, # Parts per million
    "42101 CO": 1, # Parts per million
    "42401 SO2": 0.001, # Parts per billion
    "42602 NO2": 0.001 # Parts per billion
}

LABELS = {
    '81102': 'PM10',
    '42401 SO2': 'SO2',
    '42101 CO': 'CO',
    '42602 NO2': 'NO2',
    '44201 Ozone': 'O3'
}

def calculate_single_aqi(y, target):
    if y.shape[0]>0:
        a = np.interp(y, PSI_BREAKPOINTS[target][0], PSI_BREAKPOINTS[target][1])
        return a
    else:
        return None


def extract_sample(df, y_label):
    x_label = 'dt'
    x = np.arange(1, 24, 1)
    xx = np.array([df[x_label]-datetime.timedelta(hours=int(a)) for a in x])
    yy  = np.array([float(df[f'{y_label}_{int(a)}']) for a in x])
    return xx, yy

def calculate_aqi(df, site_id):
    fig, a =  plt.subplots(1,1, figsize=(10, 5))
    all_indexes = []
    all_names = []
    for polutant in PSI_BREAKPOINTS:
        print(polutant)
        xx, yy = extract_sample(df[:1], polutant)
        scale = scales[polutant]
        sub_aqindex = calculate_single_aqi(yy*scale, polutant)
        all_indexes.append(sub_aqindex)
        all_names.append(polutant)
    aqi_all = np.max(all_indexes, axis=0)
    a.plot(xx, aqi_all, c='r', label='AQI max', linewidth=5.0)

    aqi_mean = np.mean(all_indexes, axis=0)
    a.plot(xx, aqi_mean, c='k', label='AQI mean', linewidth=5.0)

    for sub_aqindex, polutant in zip(all_indexes, all_names):
        a.plot(xx, sub_aqindex, label=LABELS[polutant])
   
    a.grid()
    a.set_title(f'24h AQI ')
    a.set_xlabel('time')
    a.set_ylabel('AQI')
    a.legend(loc='lower right')
    plt.savefig(f'imgs/{site_id} AQI_time_24.jpeg')


def aqi_plot():
    fig, a =  plt.subplots(1,1, figsize=(10, 5))
    for polutant in PSI_BREAKPOINTS:
        print(polutant)
        x_breakpoints = PSI_BREAKPOINTS[polutant][0]
        scale = scales[polutant]
        scale = 1
        if polutant == '81102':
            scale = 0.01
        if polutant == '42101 CO':
            scale = 0.1
        if polutant == '44201 Ozone':
            scale = 1
        if polutant == '44201 Ozone':
            scale = 10
        xx = np.linspace(min(x_breakpoints), max(x_breakpoints), 500)
        yy = np.interp(xx, PSI_BREAKPOINTS[polutant][0], PSI_BREAKPOINTS[polutant][1])
        a.plot(xx*scale, yy, label=f'{LABELS[polutant]} {scale} {units[polutant]}', linewidth=1)
    a.grid()
    a.set_title(f' PSI functions')
    a.set_xlabel('concetrations')
    a.set_ylabel('PSI value')
    a.legend(loc='lower right')
    plt.savefig(f'imgs/PSI_functions.jpeg')

if __name__ == "__main__":
    aqi_plot()
