import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import datetime

def show_signal(df, x_label, y_label, site_id):
    x = df[x_label]
    y = df[y_label]
    fig, a =  plt.subplots(1,1, figsize=(10, 5))
    a.plot(x, y, label=y_label)

    a.grid()
    a.set_title(f'{y_label} on {site_id}')
    a.set_xlabel('time')
    a.set_ylabel(y_label)
    a.legend(loc='upper left')
    plt.savefig(f'imgs/{site_id} {y_label}_time.jpeg')


def show_sample(df, x_label, y_label, site_id):
    x = np.arange(1, 24, 1)
    xx = [df[x_label]-datetime.timedelta(hours=int(a)) for a in x]
    y = [float(df[f'{y_label}_{int(a)}']) for a in x]
    fig, a =  plt.subplots(1,1, figsize=(10, 5))
    a.plot(xx, y, label=y_label)
    a.grid()
    a.set_title(f'{y_label} on {site_id}')
    a.set_xlabel('time')
    a.set_ylabel(y_label)
    a.legend(loc='upper left')
    plt.savefig(f'imgs/{site_id} {y_label}_time_24.jpeg')


def show_histogram(df, y_label, site_id):
    values = df[y_label]
    fig, a =  plt.subplots(1,1, figsize=(10, 5))
    a.hist(values, bins=100)
    a.grid()
    a.set_title(f'{y_label} histogram {site_id}')
    a.set_ylabel('frequency')
    a.set_xlabel('concetration')

    filename = y_label.replace('/', '_')
    plt.savefig(f'imgs/{site_id} {filename}_hist.jpeg')


def fuzzy_values(x, point):
    class LinearFuzzyPoint():
        def __init__(self, point):
            self.point = point

        def membership_value(self, x):
            y = (1-np.abs(x-self.point[0])/self.point[1])
            y = np.clip(y, 0, 1)
            return y
    lfp = LinearFuzzyPoint(point)
    return lfp.membership_value(x)


def show_fuzzy_point():
    x_min = 0
    x_max = 500
    x = np.linspace(x_min, x_max, 1000)
    # 0, 500, [50, 50], 'Arround 50'
    fig, a =  plt.subplots(1,1, figsize=(10, 5))
    y = np.interp(x, [0, 20, 30, 500], [1, 1, 0, 0])
    a.plot(x, y, label='Very Low')
    a.plot(x, fuzzy_values(x, [(25+50)/2, 35]), label='Low')
    a.plot(x, fuzzy_values(x, [(50+75)/2, 35]), label='Medium')
    a.plot(x, fuzzy_values(x, [(75+100)/2, 40]), label='High')

    y = np.interp(x, [0, 90, 120, 500], [0, 0, 1, 1])
    a.plot(x, y, label='Very High')
    a.grid()
    a.set_title(f'Fuzzy CAQI ')
    a.set_ylabel('membership value')
    a.set_xlabel('index value')
    plt.legend(loc='upper right')
    plt.savefig(f'imgs/PM10_fuzzy_point.jpeg')


def gdas_table():
    def GDAS_labels():
        table = """
            Pressure at surface	hPa	PRSS	S1
            Pressure reduced to mean sea level	hPa	MSLP	S2
            Accumulated precipitation (6 h accumulation)	m	TPP6	S3
            u-component of momentum flux (3- or 6-h average)	N/m2	UMOF	S4
            v-component of momentum flux (3- or 6-h average)	N/m2	VMOF	S5
            Sensible heat net flux at surface (3- or 6-h average)	W/m2	SHTF	S6
            Downward short wave radiation flux (3- or 6-h average)	W/m2	DSWF	S7
            Relative Humidity at 2m AGL	%	RH2M	S8
            U-component of wind at 10 m AGL	m/s	U10M	S9
            V-component of wind at 10 m AGL	m/s	V10M	S10
            Temperature at 2m AGL	K	TO2M	S11
            Total cloud cover (3- or 6-h average)	%	TCLD	S12
            Geopotential height	gpm*	SHGT	S13
            Convective available potential energy	J/Kg	CAPE	S14
            Convective inhibition	J/kg	CINH	S15
            Standard lifted index	K	LISD	S16
            Best 4-layer lifted index	K	LIB4	S17
            Planetary boundary layer height	m	PBLH	S18
            Temperature at surface	K	TMPS	S19
            Accumulated convective precipitation (6 h accumulation)	m	CPP6**	S20
            Volumetric soil moisture content	frac.	SOLM	S21
            Categorial snow (yes=1, no=0) (3- or 6-h average)		CSNO	S22
            Categorial ice (yes=1, no=0) (3- or 6-h average)		CICE	S23
            Categorial freezing rain (yes=1, no=0) (3- or 6-h average)		CFZR	S24
            Categorial rain (yes=1, no=0) (3- or 6-h average)		CRAI	S25
            Latent heat net flux at surface (3- or 6-h average)	W/m2	LHTF	S26
            Low cloud cover (3- or 6-h average)	%	LCLD	S27
            Middle cloud cover (3- or 6-h average)	%	MCLD	S28
            High cloud cover (3- or 6-h average)	%	HCLD	S29
            Geopotential height	gpm*	HGTS	U1
            Temperature	K	TEMP	U2
            U-component of wind with respect to grid	m/s	UWND	U3
            V-component of wind with respect to grid	m/s	VWND	U4
            Pressure vertical velocity	hPa/s	WWND	U5
            Relative humidity	%	RELH	U6
            """
        mapa = {}
        lines = table.split('\n')
        for line in lines:
            columns = line.split('\t')
            if len(columns)>1:
                print(columns)
                mapa[columns[2]] = columns#columns[0].strip()
        return mapa

    gdas_parameters = [
        "PRSS",
        "TPP6",
        "RH2M",
        "TO2M",
        "TCLD",
        "U10M",
        "V10M",
        "TMPS",
        "PBLH"
    ]
    gdas_mapa = GDAS_labels()
    for el in gdas_parameters:
        print(gdas_mapa[el][2], gdas_mapa[el][0].strip(), gdas_mapa[el][1].strip())



