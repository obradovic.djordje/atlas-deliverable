import numpy as np
import pandas as pd
import datetime
import matplotlib.pyplot as plt
import matplotlib as mpl

# https://www.epa.gov/criteria-air-pollutants/naaqs-table
# https://www.airnow.gov/sites/default/files/2020-05/aqi-technical-assistance-document-sept2018.pdf

# "88101 PM2.5 FRM/FEM Mass",  # 'Micrograms/cubic meter (LC)']
# "81102", # PM10  Micrograms/cubic meter (25 C)
# "44201 Ozone", # Parts per million
# "42101 CO", # Parts per million
# "42401 SO2", # Parts per billion
# "42602 NO2" # Parts per billion

PSI_BREAKPOINTS = { # 
    '81102' : [ # mikro g per m3
        [0,     50,  150,  350,  420,   500,  600],
        [0,     50,  100,  200,  300,   400,  500]
    ],
    '42401 SO2' : [ # ppm
        [0, 0.03,  0.14, 0.3, 0.6, 0.8, 1],
        [0,   50,  100,  200,  300,   400,  500],
    ],
    '42101 CO' : [ # ppm
          [0,   4.5,    9,   15,   30,    40, 50],
          [0,    50,  100,  200,  300,   400,  500]
    ],
    '42602 NO2' : [ # ppm
        [0, 0.6,  1.2, 1.6, 2],
        [0, 200,  300, 400, 500]
    ],
    '44201 Ozone' : [ # ppm
        [0, 0.06, 0.12,  0.2,  0.4, 0.5, 0.6], 
        [0,   50,  100,  200,  300, 400, 500]
    ],
}

units = {
    "81102": "Micrograms/cubic meter (25 C)",
    "44201 Ozone": "ppm",
    "42101 CO": "ppm",
    "42401 SO2": "ppb",
    "42602 NO2": "ppb"
}

scales = {
    "81102": 1, # PM10  Micrograms/cubic meter (25 C)
    "44201 Ozone": 1, # Parts per million
    "42101 CO": 1, # Parts per million
    "42401 SO2": 0.001, # Parts per billion
    "42602 NO2": 0.001 # Parts per billion
}

LABELS = {
    '81102': 'PM10',
    '42401 SO2': 'SO2',
    '42101 CO': 'CO',
    '42602 NO2': 'NO2',
    '44201 Ozone': 'O3'
}

def calculate_single_aqi(y, target):
    if y.shape[0]>0:
        a = np.interp(y, PSI_BREAKPOINTS[target][0], PSI_BREAKPOINTS[target][1])
        return a
    else:
        return None

def extract_sample(df, y_label):
    x_label = 'dt'
    x = np.arange(1, 24, 1)
    xx = np.array([df[x_label]-datetime.timedelta(hours=int(a)) for a in x])
    yy  = np.array([float(df[f'{y_label}_{int(a)}']) for a in x])
    return xx, yy

def calculate_fcaqi(df):
    FLV = {
        0: [[0, 20, 25, 500], [1, 1, 0, 0]],
        1: [[0, 20, 30, 45, 55, 500], [0, 0, 1, 1, 0, 0]],
        2: [[0, 45, 55, 70, 80, 500], [0, 0, 1, 1, 0, 0]],
        3: [[0, 65, 75, 95, 105, 500], [0, 0, 1, 1, 0, 0]],
        4: [[0, 95,  105, 500], [0, 0, 1, 1]]
    }
    yy = df['AQI']
    for cc in FLV:
        df[f'FCAQI_{cc}'] = np.interp(yy, FLV[cc][0], FLV[cc][1])

def calculate_PSI(df, site_id):
    m = None
    for polutant in PSI_BREAKPOINTS:
        y = df[polutant]*scales[polutant]
        yy = calculate_single_aqi(y, polutant)
        df[f'{polutant}_PSI'] = yy
        if m is None:
            m = yy
        else:
            m = np.max([m, yy], axis=0)
    df['AQI'] = m
    caqi = np.interp(m, [0, 25, 50, 75, 100, 500], [0, 1, 2, 3, 4, 5]).astype(np.int16)
    df['CAQI'] = caqi
    calculate_fcaqi(df)



if __name__ == "__main__":
    a = np.array([1, 2, 3, 4, 5, 6])
    b = np.array([6, 5, 4, 3, 2, 1])
    c = np.max([a, b], axis=0)
    print(c)

