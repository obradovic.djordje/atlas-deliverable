import pandas as pd
import numpy as np
import os
import yaml
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from xgboost import XGBRegressor
import shap

import matplotlib.pyplot as plt

MODEL = 'xgboost'
# MODEL = 'nn'

PREDICTORS = [
                "62101 TEMP",
                "62201 WIND",
                "88101 PM2.5 FRM/FEM Mass",
                "44201 Ozone",
                "42602 NO2",
                "61103",
                "61104",
                "43202 Ethane",
                "45202 Toluene",
                "45204 o-Xylene"
            ]


class LinearFuzzyPoint():

    def __init__(self, name, point):
        self.name = name
        self.point = point

    def membership_value(self, x):
        y = (1-np.abs(x-self.point[0])/self.point[1])
        y = np.clip(y, 0, 1)
        return y

class Model():

    def __init__(self, model_type):
        self.model_type = model_type
        if self.model_type == 'xgboost':
            self.model = XGBRegressor(
                                # # silent=False, 
                                # # scale_pos_weight=1,
                                # learning_rate=0.01,  
                                # # # colsample_bytree = 0.4,
                                # subsample = 0.8,
                                # # objective='binary:logistic', 
                                n_estimators=1000, 
                                # reg_alpha = 0.3,
                                # max_depth=4, 
                                # gamma=10
            )
        if self.model_type == 'nn':
            input_shape = len(PREDICTORS)
            output_shape = 1
            self.model = keras.Sequential(
                [
                    keras.Input(shape=input_shape),
                    layers.Dropout(0.1),
                    layers.Dense(25, activation="relu"),
                    layers.Dense(output_shape, activation="sigmoid"),
                ]
            )
            self.model.compile(loss="mse", optimizer="sgd")

    def fit(self, x_train, y_train):
        if self.model_type == 'nn':
            self.model.fit(x_train, y_train, batch_size=100, epochs=5000, validation_split=0.2, verbose=1)
        else:
            self.model.fit(x_train, y_train)


    def predict(self, x):
        return self.model.predict(x)

class Experiment():

    def __init__(self, id, model_name):
        self.id = id
        self.data_frame = None
        self.model_name = model_name
        self.predictors = PREDICTORS
        self.targets = [
                "45201 Benzene",
            ]
        self.model = None
        self.valid = False

    def init_data(self):
        srednje = LinearFuzzyPoint('srednje', [5, 2])
        df = self.data_frame.dropna()
        print(df.describe())
        df['target'] = df[self.targets[0]]
        # df.target = srednje.membership_value(df['target'])
        df = df[df.target>0]
        if df.index.shape[0]>100:
            self.valid = True

        if self.valid:
            train, test = train_test_split(df, test_size=0.2)
            
            def normalize(arr):
                scale = 1 #arr.max()
                # if MODEL == 'nn':
                # scale = arr.max()
                return tf.convert_to_tensor(arr/scale) 

            self.x_train = normalize(train[self.predictors])
            self.y_train = normalize(train['target'])
            self.x_test  = normalize(test[self.predictors])
            self.y_test  = normalize(test['target'])
            print('x_train:', self.x_train.shape)
            print('y_train:', self.y_train.shape)
            print('x_test:', self.x_test.shape)
            print('y_test:', self.y_test.shape)

            self.df = df

    def explore_dataframe(self):
        if self.valid:
            fig, a =  plt.subplots(1,1, figsize=(10, 5))

            a.hist(self.y_train.numpy(), bins=100)
            a.hist(self.y_test.numpy(), bins=100)
            a.grid()
            a.set_title('Target histogram')
            a.set_ylabel('frequency')
            a.set_xlabel('concetration')

            plt.savefig(f'histogram_{self.id}.jpeg')
            print(self.df.describe())

    def fit(self):
        if self.valid:
            self.model = Model(self.model_name)
            self.model.fit(self.x_train, self.y_train)
    
    def evaluate(self):
        if self.valid:
            y_predicted_test = self.model.predict(self.x_test)
            y_predicted_train = self.model.predict(self.x_train)
            
            print("Model error =", np.linalg.norm(self.y_test-y_predicted_test))
            # print(self.model.model.get_dump(with_stats=True)[0])


            explainer = shap.TreeExplainer(self.model.model)
            shap_values = explainer.shap_values(self.x_test)
            # shap.summary_plot(shap_values, self.x_test)
            fig, a =  plt.subplots(1,1, figsize=(10, 5))
            shap.summary_plot(shap_values,  self.x_test.numpy(), plot_type="layered_violin", color='#cccccc', show=False)
            plt.savefig(f'shap_{self.id}_{self.model_name}.jpeg')
            # for e in shap_values:
            #     print(e)
            # print(len(shap_values), self.x_test.shape)


            # fig, a =  plt.subplots(1,1, figsize=(10, 5))
            # xs = np.arange(0, self.x_test.shape[0])
            # for i in range(self.x_test.shape[1]):
            #     a.scatter(xs, shap_values[:, i], label=PREDICTORS[i])

            # a.grid()
            # a.set_title(f'{self.site_id}-{self.year}-{self.model_name}')
            # a.set_xlabel('primer')
            # a.set_ylabel('uticaj')
            # a.legend(loc='upper left')
            # plt.savefig(f'shap_{self.id}_{self.model_name}.jpeg')


            # accuracy = np.mean(np.abs(self.y_test-y_predicted_xgb_test))
            # accuracy_std = np.std(np.abs(self.y_test-y_predicted_xgb_test))
            # accuracy_max = np.max(np.abs(self.y_test-y_predicted_xgb_test))
            # print(f'accuracy: {accuracy:2.3f}\t{accuracy_std:2.3f}\t{accuracy_max:2.3f}')

            fig, a =  plt.subplots(1,1, figsize=(10, 5))
            a.scatter(self.y_train, self.y_train, label="x=y")
            a.scatter(self.y_train, y_predicted_train, label="train")
            a.scatter(self.y_test, y_predicted_test, label="test")

            a.grid()
            a.set_title(f'{self.site_id}-{self.year}-{self.model_name}')
            a.set_xlabel('ocekivano')
            a.set_ylabel('izracunato')
            a.legend(loc='upper left')
            plt.savefig(f'accuracy_{self.id}_{self.model_name}.jpeg')


def test():
    stanice = pd.read_csv("../epa/data/usa_btex_sites.csv")
    stanice_map = {}
    for ind in stanice.index:
        stanice_map[stanice.id[ind]] = []
    folder = "/home/atlas/data/atlas_data/merged"
    site_folder = os.listdir(folder)

    c = 0
    for site_id in site_folder:
        print(site_id)
        for el in os.listdir(os.path.join(folder, site_id)):
            year = int(el.replace('.csv', ''))
            stanice_map[site_id].append(year)
            print(c, "        ", year)
            c += 1

    c = 0
    for ind in stanice.index:
        site_id = stanice.id[ind]
        years = stanice_map[site_id]
        years = sorted(years)
        print(site_id)
        for year in years:
            filename = os.path.join(folder, f'{site_id}', f'{year}.csv')
            exp = Experiment(c, MODEL)
            exp.data_frame = pd.read_csv(filename)
            predictors_count = 0
            # for el in exp.data_frame.columns:
            #     print(el)
            # print("-----------------")
            for cc in exp.predictors:
                if cc in exp.data_frame.columns:
                    predictors_count += 1
            targets_count = 0
            for cc in exp.targets:
                if cc in exp.data_frame.columns:
                    targets_count += 1
            if predictors_count == len(exp.predictors) and targets_count == len(exp.targets):
                print(f'\t{year}\t{predictors_count}\t{targets_count}\t{len(exp.data_frame.columns)}')
                exp.init_data()
                exp.fit()
                exp.site_id = site_id
                exp.year = year
                exp.evaluate()
                # exp.explore_dataframe()
                c += 1
                if c > 10:
                    exit()


def linear_point_test():
    srednje = LinearPoint('srednje', [10, 5])
    x = np.arange(0, 15, 0.1)   

    fig, axs = plt.subplots(figsize=(10, 5))
    axs.grid()
    axs.set_xlabel('x')
    axs.set_ylabel('y')
    plt.plot(x, srednje.membership_value( x))
    x0 = [6, 12]
    plt.scatter(x0, srednje.membership_value(np.array(x0)))
    plt.savefig('linear_point.jpeg')


test()
# linear_point_test()