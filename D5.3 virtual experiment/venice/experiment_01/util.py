import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl


def describe_df(df):
    print(df.shape)
    for el in df:
        print(el)
    print(df.describe().T)

def balance_df(df, target, num):
    df0 = df[df[target] == 0].copy()
    df1 = df[df[target] == 1].copy()
    for i in range(num):
        ind0 = df0.sample(1)
        ind1 = df1.sample(1)
        df.loc[ind0.index, :] = ind1.values
    return df


def regression_error(y_test, y_predicted_test, title):
    fig, a =  plt.subplots(1,1, figsize=(10, 5))
    a.scatter(y_test, y_predicted_test, label="test")
    a.scatter(y_test, y_test, label="x=y")
    err = (y_test - y_predicted_test).abs().mean()
    err_std = (y_test - y_predicted_test).abs().std()
    print('err: ', err, err_std)
    a.plot(y_test, y_test+err+2*err_std, label="upper err")
    a.plot(y_test, y_test-err-2*err_std, label="lower err")

    a.grid()
    a.set_title(f'{title}')
    a.set_xlabel('true')
    a.set_ylabel('expected')
    a.legend(loc='upper left')
    plt.savefig(f'imgs/{title}.jpeg')

def show_signal(df, x_label, y_label):
    x = df[x_label]
    y = df[y_label]
    fig, a =  plt.subplots(1,1, figsize=(10, 5))
    a.plot(x, y, label="test")

    a.grid()
    a.set_title(f'{y_label}')
    a.set_xlabel('time')
    a.set_ylabel(y_label)
    a.legend(loc='upper left')
    plt.savefig(f'imgs/{y_label}_time.jpeg')