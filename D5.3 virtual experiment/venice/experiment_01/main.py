
import util
import aqi
import models


# https://www.epa.gov/criteria-air-pollutants/naaqs-table
# https://www.airnow.gov/sites/default/files/2020-05/aqi-technical-assistance-document-sept2018.pdf

import pandas as pd
import numpy as np

site_ids = ['11-001-0043', '13-089-0002']
for site_id in site_ids:
    df = pd.read_csv(f"data/{site_id}_1.csv")
    df.dt = pd.to_datetime(df.dt)
    df['h'] = pd.to_datetime(df.dt).dt.hour

    target = '44201 Ozone'
    # target = '42602 NO2'
    aqi.calculate_aqi(df, target)
    # df = util.balance_df(df, '44201 Ozone CAQI', 1000)
    # models.experiment_generator(site_id, df, f'{target} CAQI', True)
    # models.experiment_generator(site_id, df, f'{target} AQI', 300, False)

    aqi.calculate_faqi(df, target, [80, 50])
    util.show_signal(df[:24*15], 'dt', f'{target} AQI')
    util.show_signal(df[:24*15], 'dt', f'irradiance')
    util.show_signal(df[:24*15], 'dt', '44201 Ozone')
    aqi.translate_to_FAQI(df[:24*15], '44201 Ozone')
    aqi.show_plot('44201 Ozone')
    # aqi.calculate_faqi_2(df, target, [[0., 30., 55., 80.],[0., 0., 1., 1.]])
    # models.experiment_generator(site_id, df[(df[f'{target} FAQI']>40) & (df[f'{target} FAQI']<60)], f'{target} FAQI', 300, False)
    # models.experiment_generator(site_id, df, f'{target} FAQI', 300, False)



    # util.describe_df(df)