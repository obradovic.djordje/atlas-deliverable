import numpy as np

class LinearFuzzyPoint():

    def __init__(self, name, point):
        self.name = name
        self.point = point

    def membership_value(self, x):
        y = (1-np.abs(x-self.point[0])/self.point[1])
        y = np.clip(y, 0, 1)
        return y
