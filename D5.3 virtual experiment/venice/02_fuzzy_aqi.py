import pandas as pd
import numpy as np
import os
import yaml

import matplotlib.pyplot as plt
import matplotlib as mpl

from xgboost import XGBRegressor
from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix

# https://www.epa.gov/criteria-air-pollutants/naaqs-table
# https://www.airnow.gov/sites/default/files/2020-05/aqi-technical-assistance-document-sept2018.pdf

AQI = [
    '88101 PM2.5 FRM/FEM Mass', # PM2.5 particle pollution
    '81102', # PM10 mass
    '44201 Ozone', # ozone
    '42101 CO', # carbon monoxide
    '42401 SO2', # sulfor dioxide
    '42602 NO2', # nitrogen dioxide
]

predictors = [
    "PRSS",
    "TPP6",
    "RH2M",
    "T02M",
    "TCLD",
    "U10M",
    "V10M",
    "TMPS",
    "PBLH",
    "irradiance",
    "elevation",
    # "land_use",
    # "location_setting"
]

ESTIMATORS = 500

class LinearFuzzyPoint():

    def __init__(self, name, point):
        self.name = name
        self.point = point

    def membership_value(self, x):
        y = (1-np.abs(x-self.point[0])/self.point[1])
        y = np.clip(y, 0, 1)
        return y

def experiment(dd, predictors, target, title):
    # model = XGBRegressor(
    model = XGBClassifier(
                use_label_encoder=False,
                # # silent=False, 
                # scale_pos_weight=0.5,
                # learning_rate=0.01,  
                # # # colsample_bytree = 0.4,
                # subsample = 0.8,
                # objective='binary:logistic', 
                n_estimators=ESTIMATORS, 
                # reg_alpha = 0.3,
                max_depth=4, 
                # gamma=10
    )
    train, test = train_test_split(dd, test_size=0.2)
    x_train = train[predictors]
    y_train = train[target]
    x_test = test[predictors]
    y_test = test[target]
    eval_set = [(x_test, y_test)]
    model.fit(x_train, y_train, eval_metric="error", eval_set=eval_set, verbose=False)
    y_predicted_test = model.predict(x_test)
    y_predicted_train = model.predict(x_train)
    # fig, a =  plt.subplots(1,1, figsize=(10, 5))
    # a.scatter(y_train, y_train, label="x=y")
    # a.scatter(y_train, y_predicted_train, label="train")
    # a.scatter(y_test, y_predicted_test, label="test")

    # train_err = (y_test - y_predicted_test).abs().mean()
    # train_err_std = (y_test - y_predicted_test).abs().std()
    # print(train_err)
    # a.plot(y_train, y_train+train_err+2*train_err_std, label="x=y")
    # a.plot(y_train, y_train-train_err-2*train_err_std, label="x=y")
    # a.grid()
    # a.set_title(f'{title}')
    # a.set_xlabel('ocekivano')
    # a.set_ylabel('izracunato')
    # a.legend(loc='upper left')
    # plt.savefig(f'imgs/xgboost_{title}.jpeg')

    p = confusion_matrix(y_test, y_predicted_test)
    print(p)

    p = confusion_matrix(y_train, y_predicted_train)
    print(p)


def fuzzy_experiment(dd, predictors, target, title):
    lfp = LinearFuzzyPoint('visoko', [70, 40])
    dd.target = lfp.membership_value(dd[target])
    dd = dd[dd.target>0]
    model = XGBRegressor(
                # # silent=False, 
                # # scale_pos_weight=1,
                # learning_rate=0.01,  
                # # # colsample_bytree = 0.4,
                # subsample = 0.8,
                # objective='binary:logistic', 
                n_estimators=ESTIMATORS, 
                # reg_alpha = 0.3,
                max_depth=4, 
                # gamma=10
    )
    train, test = train_test_split(dd, test_size=0.2)
    x_train = train[predictors]
    y_train = train[target]
    x_test = test[predictors]
    y_test = test[target]
    
    eval_set = [(x_test, y_test)]
    model.fit(x_train, y_train, eval_metric="error", eval_set=eval_set, verbose=False)
    y_predicted_test = model.predict(x_test)
    y_predicted_train = model.predict(x_train)
    fig, a =  plt.subplots(1,1, figsize=(10, 5))
    a.scatter(y_train, y_train, label="x=y")
    a.scatter(y_train, y_predicted_train, label="train")
    a.scatter(y_test, y_predicted_test, label="test")
    a.grid()
    a.set_title(f'{title}')
    a.set_xlabel('ocekivano')
    a.set_ylabel('izracunato')
    a.legend(loc='upper left')
    plt.savefig(f'imgs/xgboost_{title}_fuzzy.jpeg')

    print(y_test.shape)
    print(y_train.shape)

    # p = confusion_matrix(y_test, y_predicted_test)
    # print(p)

    # p = confusion_matrix(y_train, y_predicted_train)
    # print(p)

def GDAS_labels():
    table = """
        Pressure at surface	hPa	PRSS	S1
        Pressure reduced to mean sea level	hPa	MSLP	S2
        Accumulated precipitation (6 h accumulation)	m	TPP6	S3
        u-component of momentum flux (3- or 6-h average)	N/m2	UMOF	S4
        v-component of momentum flux (3- or 6-h average)	N/m2	VMOF	S5
        Sensible heat net flux at surface (3- or 6-h average)	W/m2	SHTF	S6
        Downward short wave radiation flux (3- or 6-h average)	W/m2	DSWF	S7
        Relative Humidity at 2m AGL	%	RH2M	S8
        U-component of wind at 10 m AGL	m/s	U10M	S9
        V-component of wind at 10 m AGL	m/s	V10M	S10
        Temperature at 2m AGL	K	TO2M	S11
        Total cloud cover (3- or 6-h average)	%	TCLD	S12
        Geopotential height	gpm*	SHGT	S13
        Convective available potential energy	J/Kg	CAPE	S14
        Convective inhibition	J/kg	CINH	S15
        Standard lifted index	K	LISD	S16
        Best 4-layer lifted index	K	LIB4	S17
        Planetary boundary layer height	m	PBLH	S18
        Temperature at surface	K	TMPS	S19
        Accumulated convective precipitation (6 h accumulation)	m	CPP6**	S20
        Volumetric soil moisture content	frac.	SOLM	S21
        Categorial snow (yes=1, no=0) (3- or 6-h average)		CSNO	S22
        Categorial ice (yes=1, no=0) (3- or 6-h average)		CICE	S23
        Categorial freezing rain (yes=1, no=0) (3- or 6-h average)		CFZR	S24
        Categorial rain (yes=1, no=0) (3- or 6-h average)		CRAI	S25
        Latent heat net flux at surface (3- or 6-h average)	W/m2	LHTF	S26
        Low cloud cover (3- or 6-h average)	%	LCLD	S27
        Middle cloud cover (3- or 6-h average)	%	MCLD	S28
        High cloud cover (3- or 6-h average)	%	HCLD	S29
        Geopotential height	gpm*	HGTS	U1
        Temperature	K	TEMP	U2
        U-component of wind with respect to grid	m/s	UWND	U3
        V-component of wind with respect to grid	m/s	VWND	U4
        Pressure vertical velocity	hPa/s	WWND	U5
        Relative humidity	%	RELH	U6
        """
    mapa = {}
    lines = table.split('\n')
    for line in lines:
        columns = line.split('\t')
        if len(columns)>1:
            print(columns)
            mapa[columns[2]] = columns[0].strip()
    return mapa

# gdas_mapa = GDAS_labels()

epa_merged = "/home/atlas/data/atlas_data/epa/merged_2"
stanice = pd.read_csv("../epa/data/usa_btex_sites.csv")
df_stanice = stanice.set_index('id')

def show_histogram(values, name):
    fig, a =  plt.subplots(1,1, figsize=(10, 5))

    a.hist(values, bins=100)
    a.grid()
    a.set_title(f'{name} histogram')
    a.set_ylabel('frequency')
    a.set_xlabel('concetration')

    plt.savefig(f'histogram_{name}.jpeg')


AQI_BREAKPOINTS = {
    '44201 Ozone' : [
        [0, 0.125, 0.164, 0.204, 0.404], 
        [0, 101,   150,    200,  300],
        [0.02, 0.02, 0.02, 0.02, 0.02,],
        [20, 20, 20, 30, 30]],
    '42101 CO' : [
          [0, 4.4, 9.4, 12.4, 15.4, 30.4, 40.4, 50.4],
          [0, 50,  100,  150,  200,  300,  400,  500],
          [0.1, 0.2, 0.4, 0.4, 0.4, 1, 2, 3],
          [10, 10,   10,  20,  20, 30, 30, 30]],
    '42401 SO2' : [
        [0, 35,  75, 185],
        [0, 50, 100, 150],
        [10, 10, 10, 10],
        [10, 10, 10, 10]]
}

def show_plot(name):
    xb = np.array(AQI_BREAKPOINTS[name][0])
    x = np.linspace(xb.min(), xb.max(), 200)

    y = np.interp(x, AQI_BREAKPOINTS[name][0], AQI_BREAKPOINTS[name][1])
    yr = np.interp(x, AQI_BREAKPOINTS[name][0], AQI_BREAKPOINTS[name][3])


    fig, a =  plt.subplots(1,1, figsize=(10, 5))
    a.plot(x, y)
    plt.plot(x, y+yr/2, linestyle=':', color='k', lw=2)
    plt.plot(x, y-yr/2, linestyle=':', color='k', lw=2)
    plt.fill_between(x, y+yr/2, y-yr/2, facecolor='yellow', alpha=0.2)

    a.grid()
    a.set_title(f'{name} AQI')
    a.set_ylabel('AQI')
    a.set_xlabel('concetration')


    for i in range(len(AQI_BREAKPOINTS[name][0])):
        x0 = AQI_BREAKPOINTS[name][0][i]
        y0 = AQI_BREAKPOINTS[name][1][i]
        xr = AQI_BREAKPOINTS[name][2][i]
        yr = AQI_BREAKPOINTS[name][3][i]
        p0 = mpl.patches.Ellipse((x0, y0), xr, yr, color='r', fill=False)
        a.add_patch(p0)

    plt.savefig(f'imgs/{name}_AQI.jpeg')

show_plot('44201 Ozone')
show_plot('42101 CO')
show_plot('42401 SO2')


df = pd.read_csv("epa_aqi_dataset.csv")
df.dt = pd.to_datetime(df.dt)
# print(df.shape)
# for el in df:
#     print(el)
# print(df.describe().T)

sites = df.site_id.unique()

name = '44201 Ozone'
# name = '42101 CO'
# name = '42401 SO2'
def caqi(x):
    arr = np.array([0, 50, 100, 150, 200, 250, 300])
    for i, g in enumerate(arr):
        if x<g:
            return i-1
    return len(arr)
vcaqi = np.vectorize(caqi)

# y = np.array([10, 20, 30, 50, 60, 70, 100])
# yy = vcaqi(y)
# print(yy)

USE_PRE = True

if USE_PRE:
    for el in AQI:
        for pre in range(1, 3):
            predictors.append(f'pre_target_{el}_{pre}')

fig, a =  plt.subplots(1,1, figsize=(10, 5))
for site_id in sites:
    for year in range(2015, 2022):
        dff = df[(df.site_id == site_id) & (df.year == year)].copy()
        # dff = df
        # x = dff.dt
        y = dff[name]
        if y.shape[0]>0:
            x = np.arange(0, y.shape[0], 1)
            yAQI = np.interp(y, AQI_BREAKPOINTS[name][0], AQI_BREAKPOINTS[name][1])
            print(site_id)

            yCAQI = vcaqi(yAQI)
            # a.plot(dff.dt, yAQI)
            # dff["target"] = yAQI #yCAQI
            dff["target"] = yCAQI
            # dff["target"] = yAQI
            # dff["target"] = y

            if USE_PRE:
                for el in AQI:
                    for pre in range(1, 4):
                        dff[f'pre_target_{el}_{pre}'] = dff[el].shift(periods=pre, fill_value=0)

            experiment(dff, predictors, 'target', f'yAQI-{site_id}-{year}_')
            dff["target"] = yAQI
            fuzzy_experiment(dff, predictors, 'target', 'yAQI')
            exit()

a.grid()
a.set_title(f'{name}')
a.set_ylabel('')
a.set_xlabel('datetime')
plt.savefig(f'imgs/{name}_sites.jpeg')

exit()


print(df['site_id'].unique())


for el in df['land_use'].unique():
    dd = df[df.land_use==el]
    print(el, dd.shape[0])

print()
for el in df['location_setting'].unique():
    dd = df[df.location_setting==el]
    print(el, dd.shape[0])

for el in df:
    if el in gdas_mapa:
        print(el, gdas_mapa[el])


# for name in AQI:
#     for el in df['land_use'].unique():
#         dd = df[df.land_use==el]
#         print(el, dd.shape[0])
#         values = dd[name]
#         print(values.shape[0])
#         title = f"{el}-{name.replace('/', '_')}"
#         show_histogram(values.values, title)

for el in AQI:
    print(el)