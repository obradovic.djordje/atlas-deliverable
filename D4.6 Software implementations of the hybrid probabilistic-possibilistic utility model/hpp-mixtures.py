# -*- coding: utf-8 -*-
"""
Created on Mon May 12 14:14:00 2022
@author: djobradovic
"""

from numpy import (array, zeros, min, max, sum, minimum, vstack)

def S(x, y):
    return max(x, y)

def T1(x, y):
    return min(x, y)


class HybridUtility():

    def __init__(self, a):
        self.a = a
    
    def evaluate(self, u1, u2, mi1, mi2):
        ret = -1
        if mi1>self.a and mi2>self.a:
            if u1>self.a and u2>self.a:
                ret = (u1*(u1-self.a)+u2*(1-u1))/(1-self.a)
            elif u1<=self.a and u2>self.a:
                self.a+(u2-self.a)*(mi2-self.a)/(1-self.a)
            elif u1>self.a and u2 <= self.a:
                ret = self.a+(u1-self.a)*(mi1-self.a)/(1-self.a)
            elif u1 <= self.a and u2 <= self.a:
                ret = max(u1, u2)
        elif mi1 <= self.a and mi2 == 1:
            if u1>self.a and u2>self.a:
                ret = u2
            elif u1 <= self.a and u2 > self.a:
                ret = u2
            elif u1 <= self.a and u2 <= self.a:
                ret = S(mi1, u2)
            elif u1 <= self.a and u2 <= self.a:
                ret = max(self.a*T1(u1/self.a, mi1/self.a), u2)
        return ret
