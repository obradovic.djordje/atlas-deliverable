#!/bin/bash
dev_env=atlas

docker stop $dev_env
docker rm $dev_env
docker build -t $dev_env . --no-cache

docker run -d -it --name $dev_env \
    --mount type=bind,source="$(pwd)",target=/home  \
    --mount type=bind,source=/home/djordje/data/tmp,target=/data  \
     $dev_env:latest
docker exec -it $dev_env /bin/bash