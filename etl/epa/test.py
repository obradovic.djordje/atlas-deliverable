import pandas as pd
import numpy as np

def ttest():
    df1 = pd.DataFrame()
    df1['a'] = [1, 1, 1]
    df1['i'] = [1, 2, 3]
    df1['b'] = ['a', 'b', 'c']
    print(df1)

    df2 = pd.DataFrame()
    df2['a'] = [1, 1, 1]
    df2['i'] = [4, 5, 6]
    df2['b'] = ['a', 'b', 'c']
    print(df2)

    # result = df1.merge(df2, on='i', how='outer', suffixes=('','_'))
    print('---------------')
    result = pd.concat([df1, df2])
    print(result)
# ttest()