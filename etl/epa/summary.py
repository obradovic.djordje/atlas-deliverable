import pandas as pd
import numpy as np


def summary():
    filename = 'metadata.csv'
    df = pd.read_csv(filename)
    print(df.shape)
    for c in df.columns:
        print(c)
    
    parameter_codes = df['Parameter Code'].unique()
    print('-----------------')
    for c in parameter_codes:
        dff = df[df['Parameter Code']==c]
        names = dff['Parameter Name'].unique()
        sites = dff['site_id'].unique()
        print(c, len(sites), names[0])
        methods = dff['Method Name'].unique()
        for m in methods:
            print(f'\t{m}')
        # vv = np.unique(dff[['Method Code', 'Method Name']].values)
        # print(f'\t{vv}')


def sites(parameter_code):
    filename = 'metadata.csv'
    df = pd.read_csv(filename)
    dff = df[df['Parameter Code']==parameter_code]
    
    dfo = dff[['year', 'shape', 'site_id', 'State Code', 'County Code', 'Latitude', 'Longitude']]
    print(dfo)


# summary()

sites(45207)