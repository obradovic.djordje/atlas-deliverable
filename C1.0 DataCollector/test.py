import pandas as pd


df = pd.read_csv('cities.csv')
df_data = pd.read_csv('data.csv')

for ind in df.index[:2000]:
    city = df.city[ind]
    df_data.loc[df_data.shape[0]] = {
        "Description": city
    }

df_data.to_csv('data2.csv')