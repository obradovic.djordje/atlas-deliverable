
from flask import Flask, request, session, g, redirect
from dash import Dash, dash_table, dcc, html, Input, Output, callback
import dash
import dash_bootstrap_components as dbc
from datetime import timedelta
import pandas as pd
import json

app = Dash(__name__, use_pages=True, pages_folder="pages", external_stylesheets=[dbc.themes.BOOTSTRAP, dbc.icons.FONT_AWESOME])
app.css.append_css({'external_url': dash.get_asset_url("custom.css")})

SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "16rem",
    "padding": "2rem 1rem",
    "background-color": "#f8f9fa",
}

CONTENT_STYLE = {
    "margin-left": "18rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}

sidebar = html.Div(
    [
        html.Img(src=dash.get_asset_url("logo.png"), height="100px"),
        html.Hr(),
        html.P(
            "Data acquisition", className="lead"
        ),
        dbc.Nav(
            [
                dbc.NavLink("Home", href="/home", active="exact"),
                dbc.NavLink("DataSet catalog", href="/catalog", active="exact"),
                dbc.NavLink("Add new dataset request", href="/map-editor", active="exact"),
            ],
            vertical=True,
            pills=True,
        ),
    ],
    style=SIDEBAR_STYLE,
)

# content = dash.page_container
content = html.Div(id="page-content", style=CONTENT_STYLE, children=[dash.page_container])

app.layout = html.Div([dcc.Location(id="url"), sidebar, content])


@app.server.route("/api/callback/")
def callback():
    code = request.args.get('code')
    data = {"code": code}
    response = app.server.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json'
    )
    return response

@app.server.route('/post', methods=['POST'])
def on_post():
    data = request.form
    df = pd.read_csv("data2.csv")
    df.loc[df.shape[0]] = {
        "Description": data["name"]
    }
    df.to_csv("data2.csv")
    print(df.to_string())
    return redirect('/catalog')

if __name__ == '__main__':
    app.run_server(debug=True, host="0.0.0.0", port=8000)

