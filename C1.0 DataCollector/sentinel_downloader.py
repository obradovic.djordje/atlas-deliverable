from sentinelhub import SHConfig
import logging
import cv2

import datetime
import os

import matplotlib.pyplot as plt
import numpy as np

import datetime

from sentinelhub import (
    CRS,
    BBox,
    DataCollection,
    DownloadRequest,
    MimeType,
    MosaickingOrder,
    SentinelHubDownloadClient,
    SentinelHubRequest,
    bbox_to_dimensions,
)

# https://docs.sentinel-hub.com/api/latest/data/sentinel-5p-l2/

client_id = "sh-9889bdf2-18e4-45d6-b9c4-34cf23fb1dbf"
client_secret = "Ip9NR146f3sjdIJg3EEPCjlWmwooRj7H"

config = SHConfig()
config.sh_client_id = client_id
config.sh_client_secret = client_secret
config.sh_base_url = 'https://sh.dataspace.copernicus.eu'
config.sh_token_url = 'https://identity.dataspace.copernicus.eu/auth/realms/CDSE/protocol/openid-connect/token'


logging.basicConfig(level=logging.DEBUG)

geo_json = {"type":"Polygon","coordinates":[[[19.219004,44.86812],[19.219004,45.722697],[21.257461,45.722697],[21.257461,44.86812],[19.219004,44.86812]]]}
geo_json_belgrade = {"type":"Polygon","coordinates":[[[18.314209,43.528638],[18.314209,46.24825],[21.626587,46.24825],[21.626587,43.528638],[18.314209,43.528638]]]}
geo_json_ukraine = {"type":"Polygon","coordinates":[[[23.663691,46.577388],[23.663690,51.743718],[39.615841,51.743718],[39.615841,46.577387],[23.663692,46.577387]]]}

geo = 'UKRAINE'
imgs = 'imgs'
video = 'video.mp4'
geo_json = geo_json_belgrade

if geo == 'UKRAINE':
    imgs = 'imgs_ukraine'
    video = 'video_ukraine.mp4'
    geo_json = geo_json_ukraine

coordinates = np.array(geo_json['coordinates'])
x0 = coordinates[0][:, 0].min()
y0 = coordinates[0][:, 1].min()
x1 = coordinates[0][:, 0].max()
y1 = coordinates[0][:, 1].max()

ns_coords_wgs84 = (x0, y0, x1, y1)

print(ns_coords_wgs84)

resolution = 1001
ns_bbox = BBox(bbox=ns_coords_wgs84, crs=CRS.WGS84)
ns_size = bbox_to_dimensions(ns_bbox, resolution=resolution)

print(f"Image shape at {resolution} m resolution: {ns_size} pixels")

evalscript_true_color = """
//VERSION=3
var minVal = 0.0;
var maxVal = 0.01;
var diff = maxVal - minVal;
const map = [
	[minVal, 0x00007f], 
	[minVal + 0.125 * diff, 0x0000ff],
	[minVal + 0.375 * diff, 0x00ffff],
	[minVal + 0.625 * diff, 0xffff00],
	[minVal + 0.875 * diff, 0xff0000],
	[maxVal, 0x7f0000]
]; 

const visualizer = new ColorRampVisualizer(map)
function setup() {
   return {
    input: ["SO2","dataMask"],
	output: [
      { id: "default", bands: 4 },
      { id: "eobrowserStats", bands: 1 },
      { id: "dataMask", bands: 1 },
    ],
  };
}

function evaluatePixel(sample) {
   const qa = sample.QUALITY_FLAGS;
   const [r, g, b] = visualizer.process(sample.SO2);
   const statsVal = isFinite(sample.SO2) ? sample.SO2 : NaN;
   return {
        default: [r, g, b, sample.dataMask],
        eobrowserStats: [statsVal],
        dataMask: [sample.dataMask],
      };
}
"""

c = 0
for d in range(1, 30):
    date = f"2024-03-{d:02d}"
    request_true_color = SentinelHubRequest(
        evalscript=evalscript_true_color,
        input_data=[
            SentinelHubRequest.input_data(
                DataCollection.SENTINEL5P.define_from("s2l1c", service_url=config.sh_base_url),
                time_interval=(date, f"2024-03-{d+1:02d}"),
            )
        ],
        responses=[SentinelHubRequest.output_response("default", MimeType.PNG)],
        bbox=ns_bbox,
        size=ns_size,
        config=config,
    )

    true_color_imgs = request_true_color.get_data()

    image = true_color_imgs[0]
    print(f"Image type: {image.shape}")

    image = cv2.cvtColor(image, cv2.COLOR_RGBA2BGRA)
    font = cv2.FONT_HERSHEY_SIMPLEX 
    org = (5, 30) 
    fontScale = 0.5
    color = (1, 255, 0, 255) 
    thickness = 2
    image = cv2.putText(image, date, org, font,  fontScale, color, thickness, cv2.LINE_AA) 

    cv2.imwrite(f'{imgs}/img_{c:06d}.png', image[:, :-1, :])
    c += 1
ffmpeg_command = "ffmpeg -y -framerate 1 -i "
ffmpeg_command += f"{imgs}/img_%06d.png -r 1 -c:v libx264 -preset slow  -profile:v high -level:v 4.0 -pix_fmt yuv420p -crf 22 " 
ffmpeg_command += video
print(ffmpeg_command)
os.system(ffmpeg_command)

