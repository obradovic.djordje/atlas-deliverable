
from flask import request
from dash import Dash, dash_table, dcc, html, Input, Output, callback
import dash
import dash_ag_grid as dag
import dash_bootstrap_components as dbc
import dash_leaflet as dl
from datetime import date

import pandas as pd
import os

from dash.exceptions import PreventUpdate
from dash_extensions.javascript import assign

dash.register_page(__name__)

df = pd.read_csv("sentinel-5p-data.csv")

# How to render geojson.
point_to_layer = assign("""function(feature, latlng, context){
    const p = feature.properties;
    if(p.type === 'circlemarker'){return L.circleMarker(latlng, radius=p._radius)}
    if(p.type === 'circle'){return L.circle(latlng, radius=p._mRadius)}
    return L.marker(latlng);
}""")

gmap = dl.Map(center=[56, 10], zoom=4, children=[
    dl.TileLayer(), dl.FeatureGroup([
        dl.EditControl(id="edit_control"), dl.Marker(position=[56, 10])]),
], style={'width': '100%', 'height': '90vh', "display": "inline-block"}, id="map"),

products = df["Product/Band"]
descriptions = df["Description"]

controls = dbc.Card(
    [
        html.Form([
            html.Div(
                [
                    dbc.Label("Product/Band"),
                    dcc.Dropdown(
                        id="x-variable",
                        options=[
                            {"label": description, "value": product} for product, description in zip(products, descriptions)
                        ],
                        value="",
                    ),
                ]
            ),
            html.Div(
                [
                    dbc.Label("From - To date"),
                    dcc.DatePickerRange(
                            id='my-date-picker-range',
                            min_date_allowed=date(2022, 1, 1),
                            max_date_allowed=date(2024, 12, 1),
                            initial_visible_month=date(2024, 7, 1),
                            end_date=date(2024, 7, 10)
                        ),
                ]
            ),
            html.Div(
                [
                    dbc.Label("Description"),
                    dbc.Input(id="cluster-count", name="name", type="text", value=""),
                ]
            ),
            dbc.Button("Submit request", outline=True, color="primary", className="me-1", type='submit')
            # html.Button('Submit', type='submit')
        ], action='/post', method='post')
    ],
    body=True,
)

layout = dbc.Container(
    [
        html.H1("Request for new Dataset"),
        html.Hr(),
        dbc.Row(
            [
                dbc.Col(controls, md=4),
                dbc.Col(gmap, md=8),
            ],
            align="center",
        ),
    ],
    fluid=True,
)


# # Create example app.
# layout = html.Div([
#         dl.Map(center=[56, 10], zoom=4, children=[
#             dl.TileLayer(), dl.FeatureGroup([
#                 dl.EditControl(id="edit_control"), dl.Marker(position=[56, 10])]),
#         ], style={'width': '100%', 'height': '50vh', "display": "inline-block"}, id="map"),
#         dcc.DatePickerRange(
#                 id='my-date-picker-range',
#                 min_date_allowed=date(1995, 8, 5),
#                 max_date_allowed=date(2017, 9, 19),
#                 initial_visible_month=date(2017, 8, 5),
#                 end_date=date(2017, 8, 25)
#             ),
#         html.Br(),
#         html.Button("Remove -> Clear all", id="clear_all")
# ])


# # Trigger mode (edit) + action (remove all)
# @callback(
#     Output("edit_control", "editToolbar"), 
#     Input("clear_all", "n_clicks"))
# def trigger_action(n_clicks):
#     return dict(mode="remove", action="clear all", n_clicks=n_clicks)  # include n_click to ensure prop changes
