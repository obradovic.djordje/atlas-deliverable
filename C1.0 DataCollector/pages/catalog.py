
from flask import request
from dash import Dash, dash_table, dcc, html, Input, Output, callback
import dash
import dash_ag_grid as dag

import pandas as pd
import os

dash.register_page(__name__)

df = pd.read_csv("data2.csv")
fv = ""

# df['Size'] = df.apply(lambda r: f"{r['size']/1024**2:2.2f}MB", axis=1)

df['link'] = df.apply(lambda r: f"[download](/map-editor)", axis=1)
df['play'] = df.apply(lambda r: f"[show](/details)", axis=1)

rain =  "![alt text: rain](https://www.ag-grid.com/example-assets/weather/rain.png)"
sun = "![alt text: sun](https://www.ag-grid.com/example-assets/weather/sun.png)"
sun = "![alt text: sun](https://www.ag-grid.com/example-assets/weather/sun.png)"


df['Comment'] = f"{rain}"
df['Download'] = f"{rain}"


columnDefs = [
    { "headerName": "ID", "field": "ID", 'filter': True, "headerCheckboxSelection": True, "checkboxSelection": True},
    {"field": "Description", 'filter': True},
    {"field": "StartDate",'filter': True},
    {"field": "EndDate",'filter': True},
    {"field": "Product",'filter': True},
    {"field": "Comment", 'filter': True, "cellRenderer": "markdown"},
    # {
    #     "field": "Comment",
    #     "cellRenderer": "DBC_Button_Simple",
    #     "cellRendererParams": {"color": "success"},
    # },
    {"field": "link", 'filter': True, "cellRenderer": "markdown"},
    {"field": "play", 'filter': True, "cellRenderer": "markdown"},
]

grid = dag.AgGrid(
    id="vi-datatable-interactivity",
    rowData=df.to_dict("records"),
    columnDefs=columnDefs,
    style={'minWidth': '100%', "height": '90vh', "overflowY":"auto"},
    dashGridOptions={
        "rowSelection": "multiple", 
        "animateRows": False,
        "enableCellTextSelection": True, 
        "ensureDomOrder": True}
)

layout = html.Div([
    grid,
    html.Div(id='vi-datatable-interactivity-container')
])

# @callback(
#     Output('vi-datatable-interactivity', 'rowData'),
#     Input('vi-input_filter', 'value')
# )
# def update_filter(filter_value):
#     files = s3_filter("s3://otto-volvo/poc-01/Out/otto-version=test/", filter_value)
#     df = pd.DataFrame(files)

#     df['Size'] = df.apply(lambda r: f"{r['size']/1024**2:2.2f}MB", axis=1)
#     df['link'] = df.apply(lambda r: f"[aa](http://3.21.139.249:3001/?video=http://3.21.139.249:8084/url?url=s3://magna-elct-fisk-ottometric-exchange/Out/otto-version=350h/4040/undestorted_raw_white.mp4&timelineItems=http://3.21.139.249:8084/url?url=Out/otto-version=350h/4040/lanes_demo_ego_left_class.json)", axis=1)
#     df['Comment'] = f"{rain} {rain} {rain} {rain} {rain}"
#     return df.to_dict('records')

# @callback(
#     Output('vi-datatable-interactivity', 'style_data_conditional'),
#     Input('vi-datatable-interactivity', 'selected_columns')
# )
# def update_styles(selected_columns):
#     return [{
#         'if': { 'column_id': i },
#         'background_color': '#D2F3FF'
#     } for i in selected_columns]

# @callback(
#     Output('vi-datatable-interactivity-container', "children"),
#     Input('vi-datatable-interactivity', "derived_virtual_data"),
#     Input('vi-datatable-interactivity', "derived_virtual_selected_rows"))
# def update_graphs(rows, derived_virtual_selected_rows):
#     global df
#     if derived_virtual_selected_rows is None:
#         derived_virtual_selected_rows = []
#     print('------------------')
#     dff = df if rows is None else pd.DataFrame(rows)
#     print(dff.to_string())
#     # dff = dff[['DTID', 'Description', 'Count']]
#     # dff.to_csv('demo2.csv', index = False)
#     # df = dff
