
from flask import request
from dash import Dash, dash_table, dcc, html, Input, Output, callback
import dash
import dash_ag_grid as dag
import dash_bootstrap_components as dbc
import dash_leaflet as dl
from datetime import date

import pandas as pd
import os

from dash.exceptions import PreventUpdate
from dash_extensions.javascript import assign

dash.register_page(__name__)


layout = [

    html.Div(id="page-content", children=[
        html.A(href="https://docs.sentinel-hub.com/api/latest/data/sentinel-5p-l2/", children=["Sentinel 5p l2"])
    ])
]