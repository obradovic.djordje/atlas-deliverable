
from flask import request
from dash import Dash, dash_table, dcc, html, Input, Output, callback
import dash
import dash_ag_grid as dag
import dash_bootstrap_components as dbc
import dash_leaflet as dl
from datetime import date
import dash_player as dp
import pandas as pd
import os

from dash.exceptions import PreventUpdate
from dash_extensions.javascript import assign

dash.register_page(__name__)

layout = [
    dp.DashPlayer(
        id="player",
        url=dash.get_asset_url("video.mp4"),
        controls=True,
        width="100%",
        height="90vh",
    )
]