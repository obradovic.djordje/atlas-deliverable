dev_env=dash-internaltool
worker_name=dash-internaltool

docker stop $worker_name
docker rm $worker_name
docker run -d -it --name $worker_name \
    -p 8008:8000 \
    --env AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
    --env AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
    --env AWS_EXECUTION_ROLE=$AWS_EXECUTION_ROLE \
    --env RDS_HOST=$RDS_HOST \
    --env DATABASE=$DATABASE \
    --env RDS_USER=$RDS_USER \
    --env RDS_PASSWORD=$RDS_PASSWORD \
    --mount type=bind,source="$(pwd)/..",target=/home/src \
    $dev_env:latest
docker exec -it $worker_name /bin/bash
# docker kill $dev_env