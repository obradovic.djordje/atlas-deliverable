#!/bin/bash
dev_env=dash-internaltool

docker stop $dev_env
docker rm $dev_env
docker build  -t $dev_env ../. -f ../Dockerfile  --no-cache